package com.solomia.decorator;

import com.solomia.factory.Wait;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Input extends AbstractElement {
    public Input(WebElement webElement) {
        super(webElement);
    }

    public void clearAndClick() {
        webElement.clear();
        webElement.click();
    }

    public void waitUntilVisibleAndSendKeys(String keys){
        Wait.until(ExpectedConditions.visibilityOf(webElement)).sendKeys(keys);
    }

    public void clearClickAndSEndKeys(String keys) {
        webElement.clear();
        webElement.click();
        webElement.sendKeys(keys);
    }

//    public Input waitUntilVisibleAndClick() {
//        Wait.until(ExpectedConditions.elementToBeClickable(webElement));
//        webElement.click();
//        return this;
//    }

}
