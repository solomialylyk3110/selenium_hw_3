package com.solomia.constants;

public class Constants {
    /** Constants for driver */
    public static final String NAME_DRIVER = "webdriver.chrome.driver";

    /** Message Constants */
    public static final String MESSAGE_SEND_TO = "solomia.lylyk@gmail.com";
    public static final String MESSAGE_SUBJECT = "PageObject";
    public static final String MESSAGE_BODY = "Hi";

    private Constants(){}
}
