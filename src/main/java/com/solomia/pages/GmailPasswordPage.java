package com.solomia.pages;

import com.solomia.decorator.Button;
import com.solomia.decorator.Input;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GmailPasswordPage extends AbstractPage {
    @FindBy(xpath = "//input[@name='password']")
    private WebElement passwordInput;

    @FindBy(id = "passwordNext")
    private WebElement passwordNext;

    private static Logger logger = LogManager.getLogger(GmailPasswordPage.class);

    public void typePasswordAndSubmit(String password) {
        logger.info("Try to type password and submit");
        new Input(passwordInput).waitUntilVisibleAndSendKeys(password);
        try {
            new Button(passwordNext).waitUntilVisibleAndClick();
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }
}
