package com.solomia.pages;

import com.solomia.decorator.CustomFieldDecorator;
import com.solomia.factory.DriverManager;
import org.openqa.selenium.support.PageFactory;

abstract public class AbstractPage {
    public AbstractPage() {
        PageFactory.initElements(DriverManager.getDriver(), this);
    }
}
