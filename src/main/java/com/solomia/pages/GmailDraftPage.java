package com.solomia.pages;

import com.solomia.constants.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class GmailDraftPage extends AbstractPage {
    private static Logger logger = LogManager.getLogger(GmailDraftPage.class);

    @FindBy(xpath = "//tr[@class='zA yO' and descendant::*[text()='" + Constants.MESSAGE_SUBJECT +"']]")
    private List<WebElement> messages;

    @FindBy(xpath = "//*[@class='T-I J-J5-Ji aoO v7 T-I-atl L3']")
    private WebElement sendButton;

    @FindBy(xpath = "//*[contains(text(),'Лист надіслано.')]")
    private WebElement toastMessage;

    public WebElement getSendButton() {
        return sendButton;
    }

    public WebElement getToastMessage() {
        return toastMessage;
    }

    public WebElement getFirstMessageWithCorrectSubjectFromConstants() {
        logger.info("Get the messsage from draft folder");
        for(WebElement element: messages){
            if(element.isDisplayed()){
                logger.info(element.getText());
                element.click();
                return element;
            }
        }
        return null;
    }
}
