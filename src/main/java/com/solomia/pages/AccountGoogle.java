package com.solomia.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AccountGoogle extends AbstractPage {

    @FindBy(css = "a.gb_D.gb_Ua.gb_i")
    private WebElement userIcon;

    @FindBy(css = "div.gb_ub.gb_vb")
    private WebElement userName;

    private static Logger logger = LogManager.getLogger(AccountGoogle.class);

    public String getUserName() {
        logger.info("Get full name from Google Account ");
        userIcon.click();
        return userName.getText();
    }
}
