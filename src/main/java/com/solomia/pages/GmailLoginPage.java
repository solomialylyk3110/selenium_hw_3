package com.solomia.pages;

import com.solomia.decorator.Button;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailLoginPage extends AbstractPage {
    @FindBy(id = "identifierId")
    private WebElement loginInput;

    @FindBy(id = "identifierNext")
    private WebElement nextButton;

    private static Logger logger = LogManager.getLogger(GmailLoginPage.class);

    public void typeLoginAndSubmit(String login) {
        logger.info("Try to type login and submit");
        loginInput.sendKeys(login);
        new Button(nextButton).click();
    }
}
