package com.solomia.factory;

import com.solomia.constants.Constants;
import com.solomia.utils.properties.ConfigProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class DriverFactory {
    private WebDriver driver;

    public WebDriver createDriver() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        System.setProperty(Constants.NAME_DRIVER, ConfigProperties.getDriverPath());
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return driver;
    }
}
