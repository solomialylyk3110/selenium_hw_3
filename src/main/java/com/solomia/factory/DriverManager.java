package com.solomia.factory;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class DriverManager {
    private static ThreadLocal<WebDriver> DRIVER_POOL = new ThreadLocal<>();
//    private static WebDriver driver = null;

    private DriverManager() {
    }

    public static WebDriver getDriver() {
        if (DRIVER_POOL.get() == null) {
            DRIVER_POOL.set(new DriverFactory().createDriver());
        }
        return DRIVER_POOL.get();
    }

    public static void OpenNewWindowAndSwitchToIt(){
        ((JavascriptExecutor) DRIVER_POOL.get()).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<String>(DRIVER_POOL.get().getWindowHandles());
        DRIVER_POOL.get().switchTo().window(tabs.get(tabs.size()-1));
    }

    public static void quit() {
        if (DRIVER_POOL.get() != null) {
            DRIVER_POOL.get().quit();
            DRIVER_POOL.set(null);
        }
    }

    public static void close() {
        if (DRIVER_POOL.get() != null) {
            DRIVER_POOL.get().close();
            DRIVER_POOL.set(null);
        }
    }
}
