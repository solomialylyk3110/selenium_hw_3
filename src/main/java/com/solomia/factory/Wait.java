package com.solomia.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.function.Function;

public  class Wait {
    public static <T> T until(Function<? super WebDriver, T> isTrue) {
        return new WebDriverWait(DriverManager.getDriver(), 20).until(isTrue);
    }
}
