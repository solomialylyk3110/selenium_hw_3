package com.solomia.business;

import com.solomia.factory.DriverManager;
import com.solomia.pages.*;
import com.solomia.utils.properties.ConfigProperties;
import com.solomia.utils.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

/**
 *
 * @version 1.0
 * @author Solomia Lylyk
 */
public class BusinessObject {
    /** Logger. Output into the console */
    private static Logger logger = LogManager.getLogger(BusinessObject.class);
    /** Create driver or get driver if instance was created before*/
    private WebDriver driver = DriverManager.getDriver();
    /** Create instance for working with Gmail Draft Folder*/
    private GmailDraftPage gmailDraftPage = new GmailDraftPage();

    /**
     * Method to log in to gmail.com
     * @param user contain info about login, password and name of user.
     */
    public void login(User user) {
        driver.get(ConfigProperties.getEmailLogInURL());
        GmailLoginPage gmailLoginPage = new GmailLoginPage();
        gmailLoginPage.typeLoginAndSubmit(user.getLogin());
        GmailPasswordPage gmailPasswordPage = new GmailPasswordPage();
        gmailPasswordPage.typePasswordAndSubmit(user.getPassword());
    }

    /**
     * Method get a full name of Google(Gmail) Account
     */
    public String getCurrentGoogleAccountName() {
        return new AccountGoogle().getUserName();
    }

    /**
     * Method creates a message and save it in the Draft Folder
     * @param  messageSendTo contains String value of recipient.
     * @param messageSubject contains String value of letter's subject.
     * @param messageBody contains a main text for sending.
     */
    public void createDraftMessage(String messageSendTo, String messageSubject, String messageBody) {
        DriverManager.OpenNewWindowAndSwitchToIt();
        driver.get(ConfigProperties.getInboxURL());
        GmailComposeMessage gmailComposeMessage = new GmailComposeMessage();
        gmailComposeMessage.composeAndSaveMessage(messageSendTo, messageSubject, messageBody);
    }

    /**
     * Method gets the last sent letter
     */
    public String getFirstMessageTextFromDraft(){
        driver.get(ConfigProperties.getDraftURL());
        gmailDraftPage.getFirstMessageWithCorrectSubjectFromConstants();
        return  gmailDraftPage.getFirstMessageWithCorrectSubjectFromConstants().getText();
    }

    /**
     * Method clicks on SEND button and if the email was sent.
     */
    public boolean clickOnSendButtonAndCheckIfSuccessfully() {
        gmailDraftPage.getSendButton().click();
       return gmailDraftPage.getToastMessage().isEnabled();
    }
}
