package com.solomia;

import com.solomia.business.BusinessObject;
import com.solomia.factory.DriverManager;
import com.solomia.utils.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.stream.Stream;

import static com.solomia.constants.Constants.*;

/**
 *
 * @version 1.0
 * @author Solomia Lylyk
 */
public class SeleniumGmailTests extends BaseTest{
    /**Logger. Output into the console */
    private static Logger logger = LogManager.getLogger(SeleniumGmailTests.class);

    /**
     * @return list of user test accounts
     */
    @DataProvider(parallel = true)
    public Iterator<Object[]> users() {
        return Stream.of(
                new Object[]{"Selenium User", "seleniumuser14@gmail.com", "selenium14_"},
                new Object[]{"Selenium User", "seleniumuser17@gmail.com", "selenium17_"},
                new Object[]{"Selenium User", "seleniumuser18@gmail.com", "selenium18_"},
                new Object[]{"Selenium User", "seleniumuser19@gmail.com", "selenium19_"},
                new Object[]{"Selenium User", "seleniumuser20@gmail.com", "selenium20_"}
        ).iterator();
    }

    /**
     * Test to log in, compose a message and save it. From Draft Folder the message can be checked and sent.
     *
     * @param name contains full name of google account
     * @param login login for google account
     * @param password password for google account
     */
    @Test(dataProvider = "users")
    public void authenticationCreateDraftAndSendLetter(String name, String login, String password) {
        WebDriver driver = DriverManager.getDriver();
        BusinessObject businessObject = new BusinessObject();
        user = new User(name, login, password);
        logger.info("Test with login and password");
        businessObject.login(user);
        Assert.assertEquals(businessObject.getCurrentGoogleAccountName(), user.getName(), "No login was performed in the system");

        logger.info("Test about compose letter and save it");
        businessObject.createDraftMessage(MESSAGE_SEND_TO, MESSAGE_SUBJECT, MESSAGE_BODY);
        logger.info("Subject test with the Draft Page");
        String textMessage = businessObject.getFirstMessageTextFromDraft();
        Assert.assertTrue(textMessage.contains(MESSAGE_SUBJECT), "There is no matching with subject title and the letter");

        Assert.assertTrue(businessObject.clickOnSendButtonAndCheckIfSuccessfully(), "Can't send the letter");
        driver.quit();
    }
}
